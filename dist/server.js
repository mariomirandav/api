"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = require("./app");
const devicesController_1 = require("./api/controllers/devicesController");
const informationController_1 = require("./api/controllers/informationController");
const usuariosController_1 = require("./api/controllers/usuariosController");
const herramientasController_1 = require("./api/controllers/herramientasController");
const epcsController_1 = require("./api/controllers/epcsController");
const herramientaTipoController_1 = require("./api/controllers/herramientaTipoController");
const custom_app = new app_1.default([
    new devicesController_1.default(),
    new informationController_1.default(),
    new usuariosController_1.default(),
    new herramientasController_1.default(),
    new epcsController_1.default(),
    new herramientaTipoController_1.default()
], 3000);
custom_app.listen();
//# sourceMappingURL=server.js.map