"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const global_config = require("./configs/config");
require("reflect-metadata");
const typeorm_1 = require("typeorm");
const PlatformTools_1 = require("typeorm/platform/PlatformTools");
class App {
    constructor(controllers, port) {
        this.app = express();
        this.port = port;
        this.inicializaMiddlewares();
        this.testConnection();
        this.inicializaControllers(controllers);
        this.maxListeners();
    }
    loggerMiddleware(request, response, next) {
        let token = request.headers['goall-access-token-tk'];
        let config = require('../config.json');
        //TODO en pruebas
        if (token != config.token) {
            console.log('error en token invalid access');
            response.send({ 'success': false, 'msg': 'invalid access' });
            return;
        }
        console.log(`${request.method} ${request.path}`);
        next();
    }
    inicializaMiddlewares() {
        this.app.use(express.json({ limit: '20mb' }));
        this.app.use(express.urlencoded({ extended: false, limit: '20mb' }));
        this.app.use(this.loggerMiddleware);
        this.app.set('jwt_key', global_config.JWT_KEY);
    }
    inicializaControllers(controllers) {
        controllers.forEach((controller) => {
            this.app.use(controller.path, controller.router);
        });
    }
    testConnection() {
        typeorm_1.createConnection().then((connection) => __awaiter(this, void 0, void 0, function* () {
            console.log('conectado ok');
            // here you can start to work with your entities
            // const photoRepository = getRepository(Photo);
            // const ph = await photoRepository.findOne(1);
            // console.log(ph)
            //ph.name = "demo";
            //photoRepository.save( ph );
        })).catch(error => console.log(error));
    }
    maxListeners() {
        const emitter = new PlatformTools_1.EventEmitter();
        emitter.setMaxListeners(50);
    }
    listen() {
        this.app.listen(this.port, () => {
            console.log(`App listening on the port ${this.port}`);
        });
    }
}
// export default new App().app
exports.default = App;
//# sourceMappingURL=app.js.map