"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const Epcs_1 = require("../Entities/Epcs");
let EpcsRepository = class EpcsRepository extends typeorm_1.Repository {
    constructor() {
        super(...arguments);
        this.get_available = () => __awaiter(this, void 0, void 0, function* () {
            let entityManager = typeorm_1.getManager();
            let query = `select * from epcs where epc not in( select epc from herramientas ) `;
            let resultado = yield entityManager.query(query);
            return resultado;
        });
        this.get_all_custom = () => __awaiter(this, void 0, void 0, function* () {
            let entityManager = typeorm_1.getManager();
            let query = `select e.id, 
                    e.epc, e.idMark, (CASE WHEN ( select count(*) from herramientas h where h.epc = e.epc  ) > 0 THEN 1 ELSE 0 END) as estado 
                    from epcs e`;
            let resultado = yield entityManager.query(query);
            return resultado;
        });
    }
};
EpcsRepository = __decorate([
    typeorm_1.EntityRepository(Epcs_1.Epcs)
], EpcsRepository);
exports.EpcsRepository = EpcsRepository;
//# sourceMappingURL=EpcsRepository.js.map