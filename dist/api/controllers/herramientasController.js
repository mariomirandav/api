"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const typeorm_1 = require("typeorm");
const HerramientasRepository_1 = require("../../Repository/HerramientasRepository");
const SyncSocket_1 = require("../../SyncSocket");
const Herramientas_1 = require("../../Entities/Herramientas");
class herramientasController {
    constructor() {
        this.path = '/herramientas';
        this.router = express.Router();
        this.startRoutes = () => {
            this.router.post('/get_all', this.get_all);
            this.router.post('/get_by_id', this.get_by_id);
            this.router.post('/get_by_epc', this.get_by_epc);
            this.router.post('/save', this.save);
            this.router.post('/delete', this.delete);
        };
        this.get_all = (request, response) => __awaiter(this, void 0, void 0, function* () {
            let herramientas = yield typeorm_1.getCustomRepository(HerramientasRepository_1.HerramientasRepository).find();
            let procesada = [];
            herramientas.forEach(element => {
                procesada.push({ id: element.id, nombre: element.nombre, idTipo: element.idTipo, epc: element.epc });
            });
            // response.send({'success': true, 'data': herramientas});
            response.send({ 'success': true, 'data': procesada });
        });
        this.get_by_epc = (request, response) => __awaiter(this, void 0, void 0, function* () {
            try {
                let herramienta = yield typeorm_1.getCustomRepository(HerramientasRepository_1.HerramientasRepository).findOneOrFail({ epc: request.body.epc });
                if (herramienta == null) {
                    response.send({ 'success': false, 'message': 'herramienta no encontrada' });
                }
                else {
                    response.send({ 'success': true, 'message': 'ok', 'data': herramienta });
                }
            }
            catch (err) {
                response.send({ 'success': false, 'message': 'herramienta no encontrada' });
            }
        });
        this.get_by_id = (request, response) => __awaiter(this, void 0, void 0, function* () {
            try {
                let herramienta = yield typeorm_1.getCustomRepository(HerramientasRepository_1.HerramientasRepository).findOneOrFail(request.body.id);
                if (herramienta == null) {
                    response.send({ 'success': false, 'message': 'herramienta no encontrada' });
                }
                else {
                    response.send({ 'success': true, 'message': 'ok', 'data': herramienta });
                }
            }
            catch (err) {
                response.send({ 'success': false, 'message': 'herramienta no encontrada' });
            }
        });
        this.delete = (request, response) => __awaiter(this, void 0, void 0, function* () {
            let datos_herramienta = yield typeorm_1.getCustomRepository(HerramientasRepository_1.HerramientasRepository).findOne({ epc: request.body.epc });
            let sync = new SyncSocket_1.SyncSocket();
            if (datos_herramienta != null) {
                datos_herramienta.remove();
                sync.envio({ task: "DROP_TOOL", data: datos_herramienta });
                response.send({ 'success': true, 'message': 'Eliminado registro' });
            }
            else {
                response.send({ 'success': false, 'message': 'Registro no encontrado' });
            }
        });
        this.save = (request, response) => __awaiter(this, void 0, void 0, function* () {
            let datos_herramienta = yield typeorm_1.getCustomRepository(HerramientasRepository_1.HerramientasRepository).findOne({ epc: request.body.epc });
            let sync = new SyncSocket_1.SyncSocket();
            if (datos_herramienta == null) {
                let herramienta_obj = new Herramientas_1.Herramientas();
                let date_transform = '';
                let date_selected = new Date();
                if (request.body.fechaVencimiento != '') {
                    date_transform = request.body.fechaVencimiento.split('/');
                    date_selected = new Date(parseInt(date_transform[2]), parseInt(date_transform[1]) - 1, parseInt(date_transform[0]));
                    let utc_offset = date_selected.getTimezoneOffset();
                    date_selected.setMinutes(date_selected.getMinutes() + utc_offset);
                }
                herramienta_obj.nombre = request.body.nombre;
                herramienta_obj.img = request.body.full_image;
                herramienta_obj.detalle = request.body.detalle;
                herramienta_obj.epc = request.body.epc;
                herramienta_obj.estado = request.body.estado;
                herramienta_obj.idTipo = request.body.idTipo;
                herramienta_obj.inventario = 0;
                herramienta_obj.puerta = 1;
                herramienta_obj.validador = 0;
                herramienta_obj.estado = 0;
                if (request.body.fechaVencimiento != '') {
                    herramienta_obj.fechaVencimiento = date_selected;
                }
                let retorno = herramienta_obj.save();
                retorno.then((herramienta) => {
                    sync.envio({ task: "CREATE_TOOL", data: herramienta });
                    response.send({ 'success': true, 'message': 'herramienta creada correctamente' + herramienta_obj.epc, 'epc': herramienta.epc });
                }).catch((err) => {
                    response.send({ 'success': false, 'message': 'no se pudo generar el usuario' + err });
                });
            }
            else {
                let date_transform = request.body.fechaVencimiento.split('/');
                let date_selected = new Date(parseInt(date_transform[2]), parseInt(date_transform[1]) - 1, parseInt(date_transform[0]));
                // convert to timezone +180 mins aprox
                let utc_offset = date_selected.getTimezoneOffset();
                date_selected.setMinutes(date_selected.getMinutes() + utc_offset);
                datos_herramienta.estado = request.body.estado;
                datos_herramienta.epc = request.body.epc;
                datos_herramienta.idTipo = request.body.idTipo;
                datos_herramienta.img = request.body.full_image;
                datos_herramienta.nombre = request.body.nombre;
                datos_herramienta.detalle = request.body.detalle;
                datos_herramienta.fechaVencimiento = date_selected;
                datos_herramienta.save();
                sync.envio({ task: "EDIT_TOOL", data: datos_herramienta });
                response.send({ 'success': true, 'message': 'usuario modificado correctamente', 'epc': datos_herramienta.epc });
            }
        });
        this.startRoutes();
    }
}
exports.default = herramientasController;
//# sourceMappingURL=herramientasController.js.map