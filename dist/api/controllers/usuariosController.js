"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const Utilities_1 = require("../../Utilities");
const typeorm_1 = require("typeorm");
const UsuariosRepository_1 = require("../../Repository/UsuariosRepository");
const Usuarios_1 = require("../../Entities/Usuarios");
const SyncSocket_1 = require("../../SyncSocket");
class usuariosController {
    constructor() {
        this.path = '/usuarios';
        this.router = express.Router();
        this.utils = new Utilities_1.Utilities();
        this.startRoutes = () => {
            this.router.post('/get_all', this.get_all);
            this.router.post('/get_by_id', this.get_by_id);
            this.router.post('/save', this.save);
            this.router.post('/delete', this.delete);
            this.router.post('/enable_disabled', this.enable_disabled);
        };
        this.get_by_id = (request, response) => __awaiter(this, void 0, void 0, function* () {
            try {
                let usuario = yield typeorm_1.getCustomRepository(UsuariosRepository_1.UsuariosRepository).findOneOrFail(request.body.id);
                if (usuario == null) {
                    response.send({ 'success': false, 'message': 'usuario no encontrado' });
                }
                else {
                    response.send({ 'success': true, 'message': 'ok', 'data': usuario });
                }
            }
            catch (err) {
                response.send({ 'success': false, 'message': 'usuario no encontrado' });
            }
        });
        this.get_all = (request, response) => __awaiter(this, void 0, void 0, function* () {
            let usuarios = yield typeorm_1.getCustomRepository(UsuariosRepository_1.UsuariosRepository).find();
            response.send({ 'success': true, 'data': usuarios });
        });
        this.delete = (request, response) => __awaiter(this, void 0, void 0, function* () {
            let datos_usuario = yield typeorm_1.getCustomRepository(UsuariosRepository_1.UsuariosRepository).findOne({ lector_identificador: request.body.id });
            console.log('Eliminado registro ' + request.body.id);
            let sync = new SyncSocket_1.SyncSocket();
            if (datos_usuario != null) {
                datos_usuario.remove();
                sync.envio({ task: "DROP_USER", data: datos_usuario });
                response.send({ 'success': true, 'message': 'Eliminado registro' });
            }
            else {
                response.send({ 'success': false, 'message': 'Registro no encontrado' });
            }
        });
        this.enable_disabled = (request, response) => __awaiter(this, void 0, void 0, function* () {
            let datos_usuario = yield typeorm_1.getCustomRepository(UsuariosRepository_1.UsuariosRepository).findOne({ lector_identificador: request.body.id });
            let sync = new SyncSocket_1.SyncSocket();
            if (datos_usuario != null) {
                datos_usuario.estado = (datos_usuario.estado == 1) ? 0 : 1;
                datos_usuario.save();
                sync.envio({ task: "ENABLED_DISABLED", data: datos_usuario });
                response.send({ 'success': true, 'message': 'Eliminado registro' });
            }
        });
        this.save = (request, response) => __awaiter(this, void 0, void 0, function* () {
            let datos_usuario = yield typeorm_1.getCustomRepository(UsuariosRepository_1.UsuariosRepository).findOne(request.body.id);
            let sync = new SyncSocket_1.SyncSocket();
            let datos_por_identificador_lector = yield typeorm_1.getCustomRepository(UsuariosRepository_1.UsuariosRepository).findOne({ lector_identificador: request.body.lector_identificador });
            if (datos_usuario == null) {
                let usuario_obj = new Usuarios_1.Usuarios();
                if (datos_por_identificador_lector != null) {
                    response.send({ 'success': false, 'message': 'El identificador del usuario ya existe' });
                }
                else {
                    usuario_obj.nombre = request.body.nombre;
                    usuario_obj.img = request.body.full_image;
                    usuario_obj.tipo = parseInt(request.body.tipo);
                    usuario_obj.lector_identificador = parseInt(request.body.lector_identificador); // usara run de persona
                    let retorno = usuario_obj.save();
                    retorno.then((usuario) => {
                        sync.envio({ task: "CREATE_USER", data: usuario });
                        response.send({ 'success': true, 'message': 'usuario creado correctamente' + usuario_obj.img, 'id': usuario.id });
                    }).catch((err) => {
                        response.send({ 'success': false, 'message': 'no se pudo generar el usuario' + err });
                    });
                }
            }
            else {
                if (datos_usuario.lector_identificador != request.body.lector_identificador) {
                    response.send({ 'success': false, 'message': 'La identificacion del usuario no corresponde' });
                }
                else {
                    console.log('0editando');
                    datos_usuario.img = request.body.full_image;
                    datos_usuario.nombre = request.body.nombre;
                    datos_usuario.tipo = parseInt(request.body.tipo);
                    datos_usuario.lector_identificador = parseInt(request.body.lector_identificador);
                    datos_usuario.save();
                    sync.envio({ task: "EDIT_USER", data: datos_usuario });
                    response.send({ 'success': true, 'message': 'usuario modificado correctamente', 'id': datos_usuario.id });
                }
            }
        });
        this.startRoutes();
    }
}
exports.default = usuariosController;
//# sourceMappingURL=usuariosController.js.map