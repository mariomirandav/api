"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
require("reflect-metadata");
const typeorm_1 = require("typeorm");
const Utilities_1 = require("../../Utilities");
const HerramientasRepository_1 = require("../../Repository/HerramientasRepository");
const Herramientas_1 = require("../../Entities/Herramientas");
const Registros_1 = require("../../Entities/Registros");
class DevicesController {
    constructor() {
        this.path = '/devices';
        this.router = express.Router();
        this.utils = new Utilities_1.Utilities();
        this.startRoutes = () => {
            this.router.post('/synchronize/registers', this.sync_register);
            // this.router.post('/synchronize/user', this.sync_user);
            this.router.post('/synchronize/tools', this.sync_tools);
        };
        this.sync_register = (request, response) => {
            this._sync_register(request);
            response.send({ 'status': true, 'message': 'ok' });
        };
        this.sync_tools = (request, response) => {
            this._sync(request);
            response.send({ 'status': true, 'message': 'ok' });
        };
        this._sync = (request) => __awaiter(this, void 0, void 0, function* () {
            // let token: string   = request.body.tkn;
            // let metodo: string  = request.body.method;
            let datos = request.body.data;
            let datos_json = JSON.parse(datos);
            for (var row in datos_json) {
                try {
                    let registro = yield typeorm_1.getCustomRepository(HerramientasRepository_1.HerramientasRepository).findOne({ epc: datos_json[row].epc });
                    if (registro == null) {
                        let herra = new Herramientas_1.Herramientas();
                        herra.id = parseInt(datos_json[row].id);
                        herra.epc = datos_json[row].epc;
                        herra.nombre = datos_json[row].nombre;
                        herra.detalle = datos_json[row].detalle;
                        herra.idTipo = parseInt(datos_json[row].idTipo);
                        herra.estado = parseInt(datos_json[row].estado);
                        herra.inventario = parseInt(datos_json[row].inventario);
                        herra.puerta = parseInt(datos_json[row].puerta);
                        herra.validador = parseInt(datos_json[row].validador);
                        herra.img = datos_json[row].img;
                        yield herra.save();
                    }
                    else {
                        registro.estado = parseInt(datos_json[row].estado);
                        registro.inventario = parseInt(datos_json[row].inventario);
                        registro.puerta = parseInt(datos_json[row].puerta);
                        registro.validador = parseInt(datos_json[row].validador);
                        registro.save();
                    }
                }
                catch (err) {
                    console.log(err);
                }
            }
        });
        this.startRoutes();
    }
    _sync_register(request) {
        let datos = request.body.data;
        let datos_json = JSON.parse(datos);
        for (var row in datos_json) {
            let registro = new Registros_1.Registros();
            let usuario = datos_json[row].usuario;
            let herramienta = datos_json[row].herramienta;
            let fecha = datos_json[row].fecha;
            let tipo_evento = datos_json[row].type_event;
            let estado = datos_json[row].estado;
            let epc = datos_json[row].epc;
            registro.estado = estado;
            registro.fecha = new Date(fecha);
            registro.herramienta = herramienta;
            registro.usuario = usuario;
            registro.type_event = tipo_evento;
            registro.epc = epc;
            registro.save();
        }
    }
}
exports.default = DevicesController;
//# sourceMappingURL=devicesController.js.map