"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
class HogarController {
    constructor() {
        this.path = '/hogar';
        this.router = express.Router();
        this.saludoHogar = (request, response) => {
            response.send('Hola mundo saludos');
        };
        this.pruebasparams = (request, response) => {
            console.log(request.body);
            response.send('x');
        };
        this.iniciaRutas();
    }
    iniciaRutas() {
        this.router.get('/', this.saludoHogar);
        this.router.post('/', this.pruebasparams);
    }
}
exports.default = HogarController;
//# sourceMappingURL=hogarController.js.map