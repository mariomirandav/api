"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const typeorm_1 = require("typeorm");
const HerramientaTipoRepository_1 = require("../../Repository/HerramientaTipoRepository");
const HerramientaTipo_1 = require("../../Entities/HerramientaTipo");
const HerramientasRepository_1 = require("../../Repository/HerramientasRepository");
class herramientaTipoControllers {
    constructor() {
        this.path = '/herramientatipo';
        this.router = express.Router();
        this.startRoutes = () => {
            this.router.post('/get_all', this.get_all);
            this.router.post('/save', this.save);
            this.router.post('/get_by_id', this.get_by_id);
            this.router.post('/delete', this.delete);
        };
        this.get_all = (request, response) => __awaiter(this, void 0, void 0, function* () {
            let datos = yield typeorm_1.getCustomRepository(HerramientaTipoRepository_1.HerramientaTipoRepository).find();
            response.send({ 'success': true, 'data': datos });
        });
        this.get_by_id = (request, response) => __awaiter(this, void 0, void 0, function* () {
            try {
                let herramienta_tipo = yield typeorm_1.getCustomRepository(HerramientaTipoRepository_1.HerramientaTipoRepository).findOneOrFail({ id: request.body.id });
                if (herramienta_tipo == null) {
                    response.send({ 'success': false, 'message': 'herramienta tipo no encontrad0' });
                }
                else {
                    response.send({ 'success': true, 'message': 'ok', 'data': herramienta_tipo });
                }
            }
            catch (err) {
                response.send({ 'success': false, 'message': 'herramienta tipo no encontrado' });
            }
        });
        this.delete = (request, response) => __awaiter(this, void 0, void 0, function* () {
            let datos_herramienta_tipo = yield typeorm_1.getCustomRepository(HerramientaTipoRepository_1.HerramientaTipoRepository).findOne({ id: request.body.id });
            if (datos_herramienta_tipo != null) {
                let en_uso = yield typeorm_1.getCustomRepository(HerramientasRepository_1.HerramientasRepository).findOne({ idTipo: datos_herramienta_tipo.id });
                if (en_uso != null) {
                    response.send({ 'success': false, 'message': 'El tipo de herramienta esta vinculado, no es posible eliminar' });
                }
                else {
                    datos_herramienta_tipo.remove();
                    response.send({ 'success': true, 'message': 'Eliminado registro' });
                }
            }
            else {
                response.send({ 'success': false, 'message': 'Registro no encontrado' });
            }
        });
        this.save = (request, response) => __awaiter(this, void 0, void 0, function* () {
            let datos_herramienta_tipo = yield typeorm_1.getCustomRepository(HerramientaTipoRepository_1.HerramientaTipoRepository).findOne({ id: request.body.id });
            if (datos_herramienta_tipo == null) {
                let herramienta_tipo_obj = new HerramientaTipo_1.HerramientaTipo();
                herramienta_tipo_obj.tipo = request.body.tipo;
                let retorno = herramienta_tipo_obj.save();
                retorno.then((herramienta_tipo) => {
                    response.send({ 'success': true, 'message': 'tipo creado correctamente' + herramienta_tipo.tipo, 'id': herramienta_tipo.id });
                }).catch((err) => {
                    response.send({ 'success': false, 'message': 'no se pudo generar el usuario' + err });
                });
            }
            else {
                datos_herramienta_tipo.tipo = request.body.tipo;
                datos_herramienta_tipo.save();
                response.send({ 'success': true, 'message': 'epc modificado correctamente', 'id': datos_herramienta_tipo.id });
            }
        });
        this.startRoutes();
    }
}
exports.default = herramientaTipoControllers;
//# sourceMappingURL=herramientaTipoController.js.map