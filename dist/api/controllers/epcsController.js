"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const typeorm_1 = require("typeorm");
const EpcsRepository_1 = require("../../Repository/EpcsRepository");
const Epcs_1 = require("../../Entities/Epcs");
const HerramientasRepository_1 = require("../../Repository/HerramientasRepository");
const SyncSocket_1 = require("../../SyncSocket");
const RegistrosRepository_1 = require("../../Repository/RegistrosRepository");
class epcsControllers {
    constructor() {
        this.path = '/epcs';
        this.router = express.Router();
        this.startRoutes = () => {
            this.router.post('/get_all', this.get_all);
            this.router.post('/get_available', this.get_available);
            this.router.post('/save', this.save);
            this.router.post('/get_by_epc', this.get_by_epc);
            this.router.post('/get_by_id', this.get_by_id);
            this.router.post('/delete', this.delete);
        };
        this.get_all = (request, response) => __awaiter(this, void 0, void 0, function* () {
            /*let epcs = await getCustomRepository(EpcsRepository).find();
            response.send({'success': true, 'data': epcs});*/
            let repository = new EpcsRepository_1.EpcsRepository();
            let datos = repository.get_all_custom();
            datos.then((data) => {
                response.send({ 'success': true, 'data': data });
            }).catch((err) => {
                console.log(err);
                response.send({ 'success': false, 'data': [] });
            });
        });
        this.get_available = (request, response) => __awaiter(this, void 0, void 0, function* () {
            let repository = new EpcsRepository_1.EpcsRepository();
            let datos = repository.get_available();
            datos.then((data) => {
                response.send({ 'success': true, 'data': data });
            }).catch((err) => {
                response.send({ 'success': false, 'data': [] });
            });
        });
        this.get_by_id = (request, response) => __awaiter(this, void 0, void 0, function* () {
            try {
                let epc = yield typeorm_1.getCustomRepository(EpcsRepository_1.EpcsRepository).findOneOrFail({ id: request.body.id });
                if (epc == null) {
                    response.send({ 'success': false, 'message': 'epc no encontrad0' });
                }
                else {
                    response.send({ 'success': true, 'message': 'ok', 'data': epc });
                }
            }
            catch (err) {
                response.send({ 'success': false, 'message': 'epc no encontrado' });
            }
        });
        this.get_by_epc = (request, response) => __awaiter(this, void 0, void 0, function* () {
            try {
                let epc = yield typeorm_1.getCustomRepository(EpcsRepository_1.EpcsRepository).findOneOrFail({ epc: request.body.epc });
                if (epc == null) {
                    response.send({ 'success': false, 'message': 'epc no encontrad0' });
                }
                else {
                    response.send({ 'success': true, 'message': 'ok', 'data': epc });
                }
            }
            catch (err) {
                response.send({ 'success': false, 'message': 'epc no encontrado' });
            }
        });
        this.delete = (request, response) => __awaiter(this, void 0, void 0, function* () {
            let datos_epc = yield typeorm_1.getCustomRepository(EpcsRepository_1.EpcsRepository).findOne({ id: request.body.id });
            if (datos_epc != null) {
                let en_uso = yield typeorm_1.getCustomRepository(HerramientasRepository_1.HerramientasRepository).findOne({ epc: datos_epc.epc });
                if (en_uso != null) {
                    response.send({ 'success': false, 'message': 'El epc esta vinculado, no es posible eliminar' });
                }
                else {
                    datos_epc.remove();
                    response.send({ 'success': true, 'message': 'Eliminado registro' });
                }
            }
            else {
                response.send({ 'success': false, 'message': 'Registro no encontrado' });
            }
        });
        this.save = (request, response) => __awaiter(this, void 0, void 0, function* () {
            let datos_epc = yield typeorm_1.getCustomRepository(EpcsRepository_1.EpcsRepository).findOne({ id: request.body.id });
            let datos_epc_existente = yield typeorm_1.getCustomRepository(EpcsRepository_1.EpcsRepository).findOne({ epc: request.body.epc });
            let datos_idmark_existente = yield typeorm_1.getCustomRepository(EpcsRepository_1.EpcsRepository).findOne({ idMark: request.body.idMark });
            if (datos_epc == null) {
                let epc_obj = new Epcs_1.Epcs();
                if (datos_epc_existente != null) {
                    response.send({ 'success': false, 'message': 'Epc ya existente' });
                }
                else if (datos_idmark_existente != null) {
                    response.send({ 'success': false, 'message': 'Mark ID ya existente' });
                }
                else {
                    epc_obj.epc = request.body.epc;
                    epc_obj.idMark = request.body.idMark;
                    let retorno = epc_obj.save();
                    retorno.then((epc) => {
                        response.send({ 'success': true, 'message': 'epc creado correctamente' + epc.epc, 'id': epc.id });
                    }).catch((err) => {
                        response.send({ 'success': false, 'message': 'no se pudo generar el usuario' + err });
                    });
                }
            }
            else {
                if (datos_epc_existente != null && datos_epc_existente.id != request.body.id) {
                    response.send({ 'success': false, 'message': 'no se pudo actualizar el epc, revise que el codigo epc no exista' });
                }
                else if (datos_idmark_existente != null && datos_idmark_existente.id != request.body.id) {
                    response.send({ 'success': false, 'message': 'no se pudo actualizar el epc por que existe la mark ID' });
                }
                else {
                    let old_epc = datos_epc.epc;
                    datos_epc.idMark = request.body.idMark;
                    datos_epc.epc = request.body.epc;
                    datos_epc.save();
                    // actualizo el epc con el nuevo si actualizo
                    let herramientas_epcs = yield typeorm_1.getCustomRepository(HerramientasRepository_1.HerramientasRepository).find({ epc: old_epc });
                    if (herramientas_epcs.length > 0) {
                        let repository = new HerramientasRepository_1.HerramientasRepository();
                        repository.update_epc(old_epc, request.body.epc);
                        let sync = new SyncSocket_1.SyncSocket();
                        sync.envio({ task: "UPDATE_EPC", data: { old_epc: old_epc, new_epc: request.body.epc } });
                        // let sync = new SyncSocket();
                        // for(var index in herramientas_epcs) {
                        //     herramientas_epcs[index].epc = request.body.epc;
                        //     herramientas_epcs[index].save();
                        //     sync.envio({task:"UPDATE_EPC", data: { old_epc: old_epc, new_epc: request.body.epc } });
                        // }
                    }
                    // actualizo los registros con el nuevo epc, cambiando el viejo para no perder informacion
                    let registros_epc = yield typeorm_1.getCustomRepository(RegistrosRepository_1.RegistrosRepository).find({ epc: old_epc });
                    if (registros_epc.length > 0) {
                        let repository = new RegistrosRepository_1.RegistrosRepository();
                        repository.update_epc(old_epc, request.body.epc);
                        // for( var index_b in registros_epc ) {
                        //     console.log(registros_epc[ index_b ].epc)
                        //     //registros_epc[ index_b ].epc = request.body.epc;
                        //     //registros_epc[ index_b ].save();
                        // }
                    }
                    response.send({ 'success': true, 'message': 'epc modificado correctamente', 'id': datos_epc.id });
                }
            }
        });
        this.startRoutes();
    }
}
exports.default = epcsControllers;
//# sourceMappingURL=epcsController.js.map