"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
require("reflect-metadata");
const Utilities_1 = require("../../Utilities");
const typeorm_1 = require("typeorm");
class informationController {
    constructor() {
        this.path = '/information';
        this.router = express.Router();
        this.utils = new Utilities_1.Utilities();
        this.startRoutes = () => {
            this.router.post('/dashboard', this.dashboard_information);
            this.router.post('/report', this.report);
        };
        this.report = (request, response) => __awaiter(this, void 0, void 0, function* () {
            let result = this._get_report(request);
            result.then((data) => {
                if (data.length == 0) {
                    response.send({ 'success': false, 'data': [] });
                }
                else {
                    response.send({ 'success': true, 'data': data });
                }
            }).catch((err) => {
                response.send({ 'success': false, 'data': [] });
            });
        });
        this.dashboard_information = (request, response) => __awaiter(this, void 0, void 0, function* () {
            let config_data = require('../../../config.json');
            let promise_master = Promise.all([
                this._get_item_disponibles(request),
                this._get_item_usados(request),
                this._get_item_inutilizables(request),
                this._get_inventario(request)
            ]);
            promise_master.then((data) => {
                let disponible = 0;
                let usados = 0;
                let inutilizados = 0;
                let inventario;
                data.forEach((item, key) => {
                    if (key === 0) {
                        disponible = item;
                    }
                    else if (key === 1) {
                        usados = item;
                    }
                    else if (key === 2) {
                        inutilizados = item;
                    }
                    else if (key === 3) {
                        inventario = item;
                    }
                });
                response.send({
                    'status': true,
                    'message': 'ok',
                    'result': {
                        disponible: disponible,
                        usado: usados,
                        inutilizados: inutilizados,
                        inventario: inventario
                    }
                });
            }).catch((err) => {
                response.send({
                    'status': false,
                    'message': err
                });
            });
        });
        this._get_item_disponibles = (request) => __awaiter(this, void 0, void 0, function* () {
            let entityManager = typeorm_1.getManager();
            let query = "SELECT COUNT(*) count FROM herramientas WHERE inventario = 1 AND estado = 0";
            let resultado = yield entityManager.query(query);
            return resultado;
        });
        this._get_item_usados = (request) => __awaiter(this, void 0, void 0, function* () {
            let entityManager = typeorm_1.getManager();
            let query = "SELECT COUNT(*) count FROM herramientas WHERE inventario = 0 AND estado = 0";
            let resultado = yield entityManager.query(query);
            return resultado;
        });
        this._get_item_inutilizables = (request) => __awaiter(this, void 0, void 0, function* () {
            let entityManager = typeorm_1.getManager();
            let query = "SELECT COUNT(*) count FROM herramientas WHERE (estado = 1 OR estado = 2) AND inventario = 1";
            let resultado = yield entityManager.query(query);
            return resultado;
        });
        this._get_door_by_type = (type) => __awaiter(this, void 0, void 0, function* () {
            let entityManager = typeorm_1.getManager();
            let query = `select DISTINCT (puerta) from herramientas h 
                where idTipo = ${type}
                and estado in(1,2) and inventario = 1
            
                union
            
            select DISTINCT(puerta) from herramientas h 
                where idTipo = ${type}
                and inventario = 1 and estado = 0`;
            let resultado = yield entityManager.query(query);
            return resultado;
        });
        this._get_inventario = (request) => __awaiter(this, void 0, void 0, function* () {
            let entityManager = typeorm_1.getManager();
            // TODO , puede mejorarse
            let query = `select 
            distinct(idTipo),
            (select top 1 nombre from herramientas h3 where h3.idTipo = h.idTipo ) as nombre,
            (select top 1 img from herramientas h3 where h3.idTipo = h.idTipo ) as img,
            ( SELECT COUNT(epc) from herramientas h2 where h2.idTipo = h.idTipo and( h2.inventario  = 1 and h2.estado = 0)) disponible,
            ( SELECT COUNT(epc) from herramientas h2 where h2.idTipo = h.idTipo and( h2.inventario  = 0 and h2.estado = 0)) nodisponible,
            ( SELECT COUNT(epc) from herramientas h2 where h2.idTipo = h.idTipo and( h2.estado in(1,2) ) ) inutilizables
            from herramientas h`;
            let resultado = yield entityManager.query(query);
            // console.log(resultado);
            for (let row in resultado) {
                let query_door = `select DISTINCT (puerta) from herramientas h 
                where idTipo = ` + resultado[row].idTipo + `
                and estado in(1,2) and inventario = 1
            
                union
            
                select DISTINCT(puerta) from herramientas h 
                    where idTipo = ` + resultado[row].idTipo + `
                    and inventario = 1 and estado = 0`;
                let result_door = yield entityManager.query(query_door);
                let puertas = [];
                for (var row_door in result_door) {
                    puertas.push((result_door[row_door].puerta == 1) ? 'A' : 'B');
                }
                resultado[row].puerta_disponibles = (puertas.length == 0) ? '-' : puertas.join('/');
            }
            return resultado;
        });
        this._get_report = (request) => __awaiter(this, void 0, void 0, function* () {
            let date_start = request.body.date_start;
            let date_end = request.body.date_end;
            let type = request.body.type;
            let date_start_format = date_start.split('/');
            let date_end_format = date_end.split('/');
            let date_start_final = date_start_format[2] + '-' + date_start_format[1] + '-' + date_start_format[0];
            let date_end_final = date_end_format[2] + '-' + date_end_format[1] + '-' + date_end_format[0];
            let entityManager = typeorm_1.getManager();
            // -- anio-mes-dia
            let query = `select
            r.epc,
            h.nombre,
            ht.tipo as 'Descripcion_Tipo' ,
            fecha as 'Fecha_Movimiento',
            r.type_event 'Evento',
            u.nombre 'Nombre_tecnico',
            u.lector_identificador,
            h.fechaVencimiento as 'Fecha_de_vencimiento_Herramienta',
            (CASE WHEN u.estado = '1' THEN 'Activo' ELSE 'Inactivo' END ) as 'Estado_Tecnico'
        from
            registros r
        left outer join herramientas h on
            r.epc = h.epc
        left outer join usuarios u on
            u.lector_identificador = r.usuario
        left join herramienta_tipo ht on
            ht.id = h.idTipo 
            
        WHERE 
        
        ht.id = ` + type + `
        
        and r.fecha BETWEEN '` + date_start_final + ` 00:00:00.000' AND '` + date_end_final + ` 23:59:59.999' 
        order by r.id DESC
        `;
            if (type == '' || type <= 0) {
                query = `select
                r.epc,
                h.nombre,
                ht.tipo as 'Descripcion_Tipo' ,
                fecha as 'Fecha_Movimiento',
                r.type_event 'Evento',
                u.nombre 'Nombre_tecnico',
                u.lector_identificador,
                h.fechaVencimiento as 'Fecha_de_vencimiento_Herramienta',
                (CASE WHEN u.estado = '1' THEN 'Activo' ELSE 'Inactivo' END ) as 'Estado_Tecnico'
            from
                registros r
            left outer join herramientas h on
                r.epc = h.epc
            left outer join usuarios u on
                u.lector_identificador = r.usuario
            left join herramienta_tipo ht on
                ht.id = h.idTipo 
                
            WHERE 
            
            
            r.fecha BETWEEN '` + date_start_final + ` 00:00:00.000' AND '` + date_end_final + ` 23:59:59.999' 
            order by r.id DESC
            `;
            }
            let result = yield entityManager.query(query);
            return result;
        });
        this.startRoutes();
    }
}
exports.default = informationController;
//# sourceMappingURL=informationController.js.map