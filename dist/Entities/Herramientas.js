"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
let Herramientas = class Herramientas extends typeorm_1.BaseEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], Herramientas.prototype, "id", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", Number)
], Herramientas.prototype, "idTipo", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Herramientas.prototype, "nombre", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Herramientas.prototype, "epc", void 0);
__decorate([
    typeorm_1.Column("nvarchar", {
        length: "max"
    }),
    __metadata("design:type", String)
], Herramientas.prototype, "img", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", Number)
], Herramientas.prototype, "inventario", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", Number)
], Herramientas.prototype, "validador", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", Number)
], Herramientas.prototype, "puerta", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", Number)
], Herramientas.prototype, "estado", void 0);
__decorate([
    typeorm_1.Column("nvarchar", {
        length: 1000
    }),
    __metadata("design:type", String)
], Herramientas.prototype, "detalle", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", Date)
], Herramientas.prototype, "fechaVencimiento", void 0);
Herramientas = __decorate([
    typeorm_1.Entity()
], Herramientas);
exports.Herramientas = Herramientas;
//# sourceMappingURL=Herramientas.js.map