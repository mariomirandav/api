import * as WebSocket from 'ws';
import { wrap } from "module";

export class SyncSocket {

    constructor(){}

    envio ( data:any ) {

        try {
            let config_data = require('../config.json');
            // let ws = new WebSocket('ws://192.168.11.8:9091');
            let ws = new WebSocket(config_data.socket_uri);
            console.log('cargando configuracion de config' + config_data.socket_uri)
            ws.on('open', function open() {
                ws.send( JSON.stringify(data));
                ws.close(); 
            });
            
            ws.on('message', function incoming(data) {
                console.log(data);
    
            });

            ws.on('error', function (err){
                // TODO, enviar notificacion???
                console.error(err);
            })
            

        }catch(err){
            console.error('Error en el socket');
        }

    }

}