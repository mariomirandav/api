import * as express from 'express';
import { getCustomRepository } from 'typeorm';
import { HerramientaTipoRepository } from '../../Repository/HerramientaTipoRepository';
import { HerramientaTipo } from '../../Entities/HerramientaTipo';
import { HerramientasRepository } from '../../Repository/HerramientasRepository';

class herramientaTipoControllers {
    public path = '/herramientatipo';
    public router = express.Router();

    constructor(){
        this.startRoutes();
    }

    public startRoutes = () => {
        this.router.post('/get_all', this.get_all);
        this.router.post('/save', this.save);
        this.router.post('/get_by_id', this.get_by_id);
        this.router.post('/delete', this.delete);
    }

    private get_all = async( request: express.Request, response: express.Response ) => {

        let datos = await getCustomRepository(HerramientaTipoRepository).find();
        
        response.send({'success': true, 'data' : datos});

    }
  
    private get_by_id = async( request : express.Request, response: express.Response ) => {
        try{
            let herramienta_tipo = await getCustomRepository(HerramientaTipoRepository).findOneOrFail({id: request.body.id});
    
            if( herramienta_tipo == null ) {
                response.send({'success': false, 'message' : 'herramienta tipo no encontrad0'});
            } else {
                response.send({'success': true, 'message' : 'ok', 'data' : herramienta_tipo});
            }
            
        }catch(err){
            response.send({'success': false, 'message' : 'herramienta tipo no encontrado'});

        }
    }

    private delete = async( request : express.Request, response: express.Response ) => {
        let datos_herramienta_tipo = await getCustomRepository(HerramientaTipoRepository).findOne({id: request.body.id});

        if( datos_herramienta_tipo != null ) {
            let en_uso = await getCustomRepository(HerramientasRepository).findOne({idTipo: datos_herramienta_tipo.id});

            if(en_uso != null) {
                response.send({'success':false, 'message' : 'El tipo de herramienta esta vinculado, no es posible eliminar'});
            } else {
                datos_herramienta_tipo.remove();
                response.send({'success':true, 'message' : 'Eliminado registro'});
            }
        } else {
            response.send({'success':false, 'message' : 'Registro no encontrado'});
        }
    }

    private save = async( request : express.Request, response: express.Response ) => {
        let datos_herramienta_tipo = await getCustomRepository(HerramientaTipoRepository).findOne({id: request.body.id});
        
        if( datos_herramienta_tipo == null ) {
            let herramienta_tipo_obj = new HerramientaTipo();
            
            herramienta_tipo_obj.tipo    = request.body.tipo;
            
            let retorno = herramienta_tipo_obj.save();
            
            retorno.then((herramienta_tipo) => {
                response.send({'success':true, 'message' : 'tipo creado correctamente' + herramienta_tipo.tipo, 'id' : herramienta_tipo.id});
            }).catch((err) => {
                response.send({'success':false, 'message' : 'no se pudo generar el usuario' + err});
            });

        } else {
            
            datos_herramienta_tipo.tipo  = request.body.tipo;

            datos_herramienta_tipo.save();

            response.send({'success':true, 'message' : 'epc modificado correctamente' , 'id' : datos_herramienta_tipo.id});

        }
    }

}

export default herramientaTipoControllers;