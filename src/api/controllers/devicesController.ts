import * as express from 'express';
import "reflect-metadata";
import { getCustomRepository} from "typeorm";
import { Utilities } from '../../Utilities';
import { HerramientasRepository } from '../../Repository/HerramientasRepository';
import { Herramientas } from '../../Entities/Herramientas';
import { Registros } from '../../Entities/Registros';

class DevicesController {
    public path   = '/devices';
    public router = express.Router();
    private utils = new Utilities();
    
    constructor() {
        this.startRoutes();
    }


    public startRoutes = () => {
        this.router.post('/synchronize/registers', this.sync_register);
        // this.router.post('/synchronize/user', this.sync_user);
        this.router.post('/synchronize/tools', this.sync_tools);
        
    }

    private sync_register = ( request: express.Request, response: express.Response ) => {
        this._sync_register(request);
        response.send( { 'status' : true, 'message' : 'ok' } );
    }


    private sync_tools = ( request: express.Request, response: express.Response ) => {
        
        this._sync(request);

        response.send( { 'status' : true, 'message' : 'ok' } );
    }


    private _sync = async ( request ) => {
        
        // let token: string   = request.body.tkn;
        // let metodo: string  = request.body.method;
        
        let datos: string   = request.body.data;


        let datos_json      = JSON.parse( datos );

        for( var row in datos_json ) {

            try{

                let registro = await getCustomRepository( HerramientasRepository ).findOne({ epc: datos_json[row].epc });
                
                if( registro == null ) {
                    let herra = new Herramientas();
                    
                    herra.id         = parseInt(datos_json[row].id);
                    herra.epc        = datos_json[row].epc;
                    herra.nombre     = datos_json[row].nombre;
                    herra.detalle    = datos_json[row].detalle;
                    herra.idTipo     = parseInt(datos_json[row].idTipo);
                    herra.estado     = parseInt(datos_json[row].estado);
                    herra.inventario = parseInt(datos_json[row].inventario);
                    herra.puerta     = parseInt(datos_json[row].puerta);
                    herra.validador  = parseInt(datos_json[row].validador);
                    herra.img        = datos_json[row].img;
                    await herra.save();
                    
                } else {
                    registro.estado     = parseInt(datos_json[row].estado);
                    registro.inventario = parseInt(datos_json[row].inventario);
                    registro.puerta     = parseInt(datos_json[row].puerta);
                    registro.validador  = parseInt(datos_json[row].validador);
                    
                    registro.save();
                }
                
            }catch(err) 
            {
                console.log(err)
            }
            

        }
    }

    private _sync_register(request) {
        let datos: string   = request.body.data;
        let datos_json      = JSON.parse( datos );


        for( var row in datos_json ) {
            
            let registro = new Registros();
            
            let usuario     = datos_json[ row ].usuario;
            let herramienta = datos_json[ row ].herramienta;
            let fecha       = datos_json[ row ].fecha;
            let tipo_evento = datos_json[ row ].type_event;
            let estado      = datos_json[ row ].estado;
            let epc         = datos_json[ row ].epc;


            registro.estado      = estado;
            registro.fecha       = new Date(fecha);
            registro.herramienta = herramienta;
            registro.usuario     = usuario;
            registro.type_event  = tipo_evento;
            registro.epc         = epc;


            registro.save();

            

        }

        
    }



}

export default DevicesController;