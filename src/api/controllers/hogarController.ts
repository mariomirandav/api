import * as express from 'express';

class HogarController {
    public path = '/hogar';
    public router = express.Router();
    
    constructor(){
        this.iniciaRutas();
    }

    public iniciaRutas(){
        this.router.get( '/', this.saludoHogar )
        this.router.post('/', this.pruebasparams);
    }

    saludoHogar = ( request: express.Request, response: express.Response ) => {
        response.send('Hola mundo saludos')
    }

    pruebasparams = ( request: express.Request, response: express.Response ) => {
        console.log( request.body )
        response.send('x');
    }
}


export default HogarController;