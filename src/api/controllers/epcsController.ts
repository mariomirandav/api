import * as express from 'express';
import { getCustomRepository } from 'typeorm';
import { EpcsRepository } from '../../Repository/EpcsRepository';
import { Epcs } from '../../Entities/Epcs';
import { HerramientasRepository } from '../../Repository/HerramientasRepository';
import { SyncSocket } from '../../SyncSocket';
import { RegistrosRepository } from '../../Repository/RegistrosRepository';

class epcsControllers {
    public path = '/epcs';
    public router = express.Router();

    constructor(){
        this.startRoutes();
    }

    public startRoutes = () => {
        this.router.post('/get_all', this.get_all);
        this.router.post('/get_available', this.get_available);
        this.router.post('/save', this.save);
        this.router.post('/get_by_epc', this.get_by_epc);
        this.router.post('/get_by_id', this.get_by_id);
        this.router.post('/delete', this.delete);
    }

    private get_all = async( request: express.Request, response: express.Response ) => {
        /*let epcs = await getCustomRepository(EpcsRepository).find();
        response.send({'success': true, 'data': epcs});*/
        let repository = new EpcsRepository();
        let datos = repository.get_all_custom();
        
        datos.then(( data ) => {
            response.send({'success': true, 'data' : data});
        }).catch((err) => {
            console.log(err);
            response.send({'success': false, 'data' : []});
        });
    }

    private get_available = async( request: express.Request, response: express.Response ) => {
        let repository = new EpcsRepository();
        let datos = repository.get_available();

        datos.then(( data ) => {
            response.send({'success': true, 'data' : data});
        }).catch((err) => {
            response.send({'success': false, 'data' : []});
        });


    }

    private get_by_id = async( request : express.Request, response: express.Response ) => {
        try{
            let epc = await getCustomRepository(EpcsRepository).findOneOrFail({id: request.body.id});
    
            if( epc == null ) {
                response.send({'success': false, 'message' : 'epc no encontrad0'});
            } else {
                response.send({'success': true, 'message' : 'ok', 'data' : epc});
            }
            
        }catch(err){
            response.send({'success': false, 'message' : 'epc no encontrado'});

        }
    }

    private get_by_epc = async( request : express.Request, response: express.Response ) => {
        try{
            let epc = await getCustomRepository(EpcsRepository).findOneOrFail({epc: request.body.epc});
    
            if( epc == null ) {
                response.send({'success': false, 'message' : 'epc no encontrad0'});
            } else {
                response.send({'success': true, 'message' : 'ok', 'data' : epc});
            }
            
        }catch(err){
            response.send({'success': false, 'message' : 'epc no encontrado'});

        }
    }

    private delete = async( request : express.Request, response: express.Response ) => {
        let datos_epc = await getCustomRepository(EpcsRepository).findOne({id: request.body.id});

        if( datos_epc != null ) {
            let en_uso = await getCustomRepository(HerramientasRepository).findOne({epc: datos_epc.epc});

            if(en_uso != null) {
                response.send({'success':false, 'message' : 'El epc esta vinculado, no es posible eliminar'});
            } else {
                datos_epc.remove();
                response.send({'success':true, 'message' : 'Eliminado registro'});
            }
        } else {
            response.send({'success':false, 'message' : 'Registro no encontrado'});
        }
    }

    private save = async( request : express.Request, response: express.Response ) => {
        let datos_epc = await getCustomRepository(EpcsRepository).findOne({id: request.body.id});
        let datos_epc_existente = await getCustomRepository(EpcsRepository).findOne({epc: request.body.epc});
        let datos_idmark_existente = await getCustomRepository(EpcsRepository).findOne({idMark: request.body.idMark});
        
        if( datos_epc == null ) {
            let epc_obj = new Epcs();
            
            if( datos_epc_existente != null ) {
                response.send({'success':false, 'message' : 'Epc ya existente'});
            } else if(datos_idmark_existente != null) {
                response.send({'success':false, 'message' : 'Mark ID ya existente'});

            } else {
                
                epc_obj.epc    = request.body.epc;
                epc_obj.idMark = request.body.idMark;
                
                let retorno = epc_obj.save();
                

                


                retorno.then((epc) => {
                    response.send({'success':true, 'message' : 'epc creado correctamente' + epc.epc, 'id' : epc.id});
                }).catch((err) => {
                    response.send({'success':false, 'message' : 'no se pudo generar el usuario' + err});
                });

            }
            
        } else {
            
            if( datos_epc_existente != null && datos_epc_existente.id != request.body.id  ) {
                
                response.send({'success':false, 'message' : 'no se pudo actualizar el epc, revise que el codigo epc no exista'});
            } else if(datos_idmark_existente != null && datos_idmark_existente.id != request.body.id) {
                
                response.send({'success':false, 'message' : 'no se pudo actualizar el epc por que existe la mark ID'});
            } else {
                let old_epc = datos_epc.epc;
                datos_epc.idMark  = request.body.idMark;
                datos_epc.epc = request.body.epc;
    
                datos_epc.save();
                // actualizo el epc con el nuevo si actualizo
                let herramientas_epcs = await getCustomRepository(HerramientasRepository).find({epc: old_epc});
                
                if( herramientas_epcs.length > 0 ) {

                    let repository = new HerramientasRepository();
                    repository.update_epc(old_epc, request.body.epc);
                    
                    let sync = new SyncSocket();
                    sync.envio({task:"UPDATE_EPC", data: { old_epc: old_epc, new_epc: request.body.epc } });

                    // let sync = new SyncSocket();
                    // for(var index in herramientas_epcs) {
                    //     herramientas_epcs[index].epc = request.body.epc;
                    //     herramientas_epcs[index].save();
                    //     sync.envio({task:"UPDATE_EPC", data: { old_epc: old_epc, new_epc: request.body.epc } });
                    // }
                }

                // actualizo los registros con el nuevo epc, cambiando el viejo para no perder informacion
                let registros_epc = await getCustomRepository(RegistrosRepository).find({epc: old_epc});

                if( registros_epc.length > 0 ) {
                    let repository = new RegistrosRepository();
                    repository.update_epc(old_epc, request.body.epc);
                    // for( var index_b in registros_epc ) {
                    //     console.log(registros_epc[ index_b ].epc)
                    //     //registros_epc[ index_b ].epc = request.body.epc;
                    //     //registros_epc[ index_b ].save();
                    // }
                }

    
                response.send({'success':true, 'message' : 'epc modificado correctamente' , 'id' : datos_epc.id});
            }

        }
    }

}

export default epcsControllers;