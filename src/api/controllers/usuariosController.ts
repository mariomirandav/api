import * as express from "express";
import { Utilities } from "../../Utilities";
import { getCustomRepository } from "typeorm";
import { UsuariosRepository } from "../../Repository/UsuariosRepository";
import { Usuarios } from "../../Entities/Usuarios";
import { SyncSocket } from "../../SyncSocket";


class usuariosController {
    public path = '/usuarios';
    public router = express.Router();
    private utils = new Utilities();
    

    constructor(){
        this.startRoutes();
    }

    public startRoutes = () => {
        this.router.post('/get_all', this.get_all);
        this.router.post('/get_by_id',this.get_by_id);
        this.router.post('/save', this.save);
        this.router.post('/delete', this.delete);
        this.router.post('/enable_disabled', this.enable_disabled);
    }

    private get_by_id = async( request: express.Request, response: express.Response ) => {

        try{
            let usuario = await getCustomRepository(UsuariosRepository).findOneOrFail(request.body.id);
    
            if( usuario == null ) {
                response.send({'success': false, 'message' : 'usuario no encontrado'});
            } else {
                response.send({'success': true, 'message' : 'ok', 'data' : usuario});
            }
            
        }catch(err){
            response.send({'success': false, 'message' : 'usuario no encontrado'});

        }

        

    }

    private get_all = async(request: express.Request, response: express.Response) => {
        
        let usuarios = await getCustomRepository(UsuariosRepository).find();
        response.send({'success':true, 'data' : usuarios});
    }

    private delete = async( request : express.Request, response: express.Response ) => {
        let datos_usuario = await getCustomRepository(UsuariosRepository).findOne({lector_identificador: request.body.id});
        console.log('Eliminado registro ' + request.body.id)
        let sync = new SyncSocket();

        if( datos_usuario != null ) {
            datos_usuario.remove();
            sync.envio({task:"DROP_USER", data: datos_usuario});
            response.send({'success':true, 'message' : 'Eliminado registro'});
        } else {
            response.send({'success':false, 'message' : 'Registro no encontrado'});
        }
    }

    private enable_disabled = async( request: express.Request, response: express.Response ) => {
        let datos_usuario = await getCustomRepository(UsuariosRepository).findOne({lector_identificador: request.body.id});
        let sync = new SyncSocket();
        if( datos_usuario != null ) {
            datos_usuario.estado = ( datos_usuario.estado == 1 ) ? 0 : 1;
            datos_usuario.save();
            sync.envio({task:"ENABLED_DISABLED", data: datos_usuario});
            response.send({'success':true, 'message' : 'Eliminado registro'});
        }
    }
    
    private save = async(request: express.Request, response: express.Response) => {
        
        let datos_usuario = await getCustomRepository(UsuariosRepository).findOne(request.body.id);
        let sync = new SyncSocket();
        let datos_por_identificador_lector = await getCustomRepository(UsuariosRepository).findOne({lector_identificador : request.body.lector_identificador});

        if( datos_usuario == null ) {
            let usuario_obj = new Usuarios();

            if( datos_por_identificador_lector != null ) {
                
                response.send({'success':false, 'message' : 'El identificador del usuario ya existe'});
            
            } else {
                usuario_obj.nombre               = request.body.nombre;
                usuario_obj.img                  = request.body.full_image;
                usuario_obj.tipo                 = parseInt(request.body.tipo);
                usuario_obj.lector_identificador = parseInt(request.body.lector_identificador);// usara run de persona
                
                let retorno = usuario_obj.save();
                
                retorno.then((usuario) => {
                    sync.envio({task:"CREATE_USER", data: usuario});
                    response.send({'success':true, 'message' : 'usuario creado correctamente' + usuario_obj.img, 'id' : usuario.id});
                }).catch((err) => {
                    response.send({'success':false, 'message' : 'no se pudo generar el usuario' + err});
                });

            }
            
        } else {
            if( datos_usuario.lector_identificador != request.body.lector_identificador ) {
                response.send({'success':false, 'message' : 'La identificacion del usuario no corresponde'});
            } else {
                console.log('0editando');
                datos_usuario.img                  = request.body.full_image;
                datos_usuario.nombre               = request.body.nombre;
                datos_usuario.tipo                 = parseInt(request.body.tipo);
                datos_usuario.lector_identificador = parseInt(request.body.lector_identificador);
    
                datos_usuario.save();
    
                
                sync.envio({task:"EDIT_USER", data: datos_usuario});
                
                response.send({'success':true, 'message' : 'usuario modificado correctamente' , 'id' : datos_usuario.id});

            }

        }




    }

}


export default usuariosController;