import * as jwt from 'jsonwebtoken';
import * as jose from 'jose';
import * as fs from 'fs';


export class Utilities{
    
    constructor(){}

    /*async decrypt_jwt_str( token:string, secret:string ): Promise<any> {
        

        var de = jwt.verify( token, secret, { algorithms: ['HS256'] }, (err, decoded) => {

            if( err ) {
                return false;
            } else {
                return decoded;
            }
    
        } );

        console.log(de)

    }*/

    _decrypt_jwt_str_normal (token, secret): any  {
        // var de = jwt.verify( token, secret, { algorithms: ['HS256'] }, (err, decoded) => {
            
        var de = jwt.verify( token, secret, { }, (err, decoded) => {
        //var de = jwt.verify( token, secret, { algorithms: ['RS256'] }, (err, decoded) => {

            if( err ) {
                return false;
            } else {
                return decoded;
            }
    
        } );

        console.log(de)

    }

    encrypt_jwt_data_normal( payload, secret: string ): any {
        let token:string = jwt.sign( payload, secret, { expiresIn : '365d'} );

        return token;
    }


    // JOSE JWT

    decrypt_jwt_str( token ) {

        var publicKey = fs.readFileSync('./certificates/public.key', 'utf-8');

        console.log(publicKey);

        //var privateKey = fs.readFileSync('./certificates/private.key', 'utf-8');
        
         var decode = jose.JWT.verify(token, publicKey,{});

         return decode;
         
    }

    decrypt_jwt_str_normal( token ) {

        console.log('decriptando');
        console.log(token);
        // var decode = jose.JWT.verify(token, Buffer.from("m4Secret",'utf-8'), { algorithms: ['HS256'] } );
        // var decode = jose.JWT.verify(token, "m4Secret", { algorithms: ['HS256'] } );
        var decode = jose.JWT.verify(token,  new Buffer('m4Secret','base64'), { algorithms: ['HS256'] } );

        return decode;
    }

    

    encrypt_jwt_data( payload ):any {
        //var publicKey = fs.readFileSync('./certificates/public.key', 'utf-8');
        var privateKey = fs.readFileSync('./certificates/private.key', 'utf-8');

        var token = jose.JWT.sign( payload, privateKey );
        
        return token;

    }

    // jwt simple

    decrypt_simple(token) {
        var jwts = require('jwt-simple');

        var decode = jwts.decode( token, 'm4Secret' );

        console.log(decode);

    }

}
