import { Column, Entity, BaseEntity, PrimaryGeneratedColumn, PrimaryColumn } from "typeorm";

@Entity()
export class Epcs extends BaseEntity {


            @PrimaryGeneratedColumn()
            id: number;
            
            @Column()
            idMark: number;

            @Column()
            epc: string;
            
}