import { Column, Entity, BaseEntity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Herramientas extends BaseEntity {


            @PrimaryGeneratedColumn()
            id: number;
            
            @Column()
            idTipo: number;
            
            @Column()
            nombre: string;
            
            @Column()
            epc: string;
            
            @Column("nvarchar",{
                length:"max"
            })
            img: string;
            
            @Column()
            inventario: number;
            
            @Column()
            validador: number;
            
            @Column()
            puerta: number;
            
            @Column()
            estado: number;
            
            @Column("nvarchar",{
                length:1000
            })
            detalle: string;

            @Column()
            fechaVencimiento:Date;
}