import { BaseEntity, PrimaryColumn, PrimaryGeneratedColumn, Column, Entity } from "typeorm";

@Entity()
export class Registros extends BaseEntity{

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    usuario: number;
    
    @Column()
    herramienta: number;
    
    @Column()
    fecha: Date;
    
    @Column()
    type_event: string;
    
    @Column()
    estado: number;

    @Column()
    epc: string;

}