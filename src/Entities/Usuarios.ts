import { Entity, BaseEntity, Column, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Usuarios extends BaseEntity{
    
    @PrimaryGeneratedColumn()
    id:number;
    
    @Column()
    nombre:string;
    
    @Column("nvarchar",{ length: "max" })
    img:string;
    
    @Column()
    tipo:number;

    @Column(
        {
            default: "0"
        }
    )
    lector_identificador: number;

    @Column({
        default: "1"
    })
    estado: number;
}