import { Column, Entity, BaseEntity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class HerramientaTipo extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;
    
    @Column()
    tipo: string;
            
}