import { Repository, EntityRepository, getManager } from "typeorm";
import { HerramientaTipo } from "../Entities/HerramientaTipo";

@EntityRepository( HerramientaTipo )
export class HerramientaTipoRepository extends Repository< HerramientaTipo >{}