import { Repository, EntityRepository, getManager } from "typeorm";
import { Epcs } from "../Entities/Epcs";

@EntityRepository( Epcs )
export class EpcsRepository extends Repository< Epcs >{
    
    public get_available = async() => {
        
        let entityManager = getManager();
        let query = `select * from epcs where epc not in( select epc from herramientas ) `;

        let resultado = await entityManager.query(query);
        return resultado;
    }

    public get_all_custom = async() => {
        let entityManager = getManager();
        let query = `select e.id, 
                    e.epc, e.idMark, (CASE WHEN ( select count(*) from herramientas h where h.epc = e.epc  ) > 0 THEN 1 ELSE 0 END) as estado 
                    from epcs e`;
        let resultado = await entityManager.query(query);
        return resultado;
    }

    
}