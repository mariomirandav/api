import { Repository, EntityRepository, getManager } from "typeorm";
import { Herramientas } from "../Entities/Herramientas";

@EntityRepository( Herramientas )
export class HerramientasRepository extends Repository< Herramientas >{
    
    public update_epc(old_epc:string, new_epc:string) {
        let entityManager = getManager();
        let query = `update herramientas set epc = '${new_epc}' where epc = '${old_epc}'`;
        entityManager.query( query );
        
    }
    
}