import { Repository, EntityRepository, getManager } from "typeorm";
import { Registros } from "../Entities/Registros";

@EntityRepository( Registros )
export class RegistrosRepository extends Repository< Registros >{
    
    public update_epc(old_epc:string, new_epc:string) {
        let entityManager = getManager();
        let query = `update registros set epc = '${new_epc}' where epc = '${old_epc}'`;

        entityManager.query( query );
        
    }

}