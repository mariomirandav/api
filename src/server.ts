import App from './app';
import devicesController from './api/controllers/devicesController';
import informationController from './api/controllers/informationController';
import usuariosController from './api/controllers/usuariosController';
import herramientasController from './api/controllers/herramientasController';
import epcsController from './api/controllers/epcsController';
import herramientaTipoController from './api/controllers/herramientaTipoController';

const custom_app = new App(
    [
        new devicesController(),
        new informationController(),
        new usuariosController(),
        new herramientasController(),
        new epcsController(),
        new herramientaTipoController()
    ],
    3000
)

custom_app.listen();