import * as express from "express";
import * as global_config from './configs/config';
import "reflect-metadata";
import {createConnection} from "typeorm";
import { EventEmitter } from "typeorm/platform/PlatformTools";

class App {
    public app: express.Application;
    public port: number;

    constructor( controllers, port ) {
        this.app = express();
        this.port = port;

        this.inicializaMiddlewares();
        this.testConnection();
        this.inicializaControllers( controllers );
        this.maxListeners();
    }

    public loggerMiddleware( request: express.Request, response: express.Response, next ) {

        let token = request.headers['goall-access-token-tk'];
        let config = require('../config.json');
        //TODO en pruebas
        if( token != config.token ) {
            console.log('error en token invalid access');
            response.send({'success' : false, 'msg': 'invalid access'});
            return;
        }

        console.log(`${request.method} ${request.path}`);
        next();
    }

    private inicializaMiddlewares() {
        
        

        this.app.use( express.json({ limit:'20mb' }) );
        this.app.use( express.urlencoded({ extended : false,limit:'20mb' }) );
        this.app.use( this.loggerMiddleware );
        this.app.set('jwt_key', global_config.JWT_KEY);
        
        
    }

    private inicializaControllers( controllers ) {
        controllers.forEach((controller) => {
            this.app.use(controller.path, controller.router);
        });
    }



    private testConnection() {
        createConnection().then(async connection => {
            console.log('conectado ok')
            // here you can start to work with your entities
            // const photoRepository = getRepository(Photo);
            // const ph = await photoRepository.findOne(1);
            // console.log(ph)
            //ph.name = "demo";
            //photoRepository.save( ph );


        }).catch(error => console.log(error));
    }

    private maxListeners() {
        const emitter = new EventEmitter();
        emitter.setMaxListeners(50);

    }

    public listen() {
        this.app.listen( this.port, () => {
            console.log(`App listening on the port ${this.port}`);
        } );
    }

}

// export default new App().app
export default App