
CREATE TABLE [dbo].[epcs](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[epc] [nvarchar](50) NULL,
	[idMark] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[herramienta_tipo]    Script Date: 3/9/2020 9:06:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[herramienta_tipo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[tipo] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[herramientas]    Script Date: 3/9/2020 9:06:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[herramientas](
	[idTipo] [int] NOT NULL,
	[nombre] [nvarchar](255) NOT NULL,
	[epc] [nvarchar](255) NOT NULL,
	[img] [nvarchar](max) NOT NULL,
	[inventario] [int] NOT NULL,
	[validador] [int] NOT NULL,
	[puerta] [int] NOT NULL,
	[estado] [int] NOT NULL,
	[detalle] [nvarchar](1000) NOT NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
	[fechaVencimiento] [datetime] NULL,
 CONSTRAINT [PK_bd66a4cd9d857c6f45c476b09b8] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[migrations]    Script Date: 3/9/2020 9:06:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[migrations](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[timestamp] [bigint] NOT NULL,
	[name] [varchar](255) NOT NULL,
 CONSTRAINT [PK_2421a75415b8a8d0f49fd67a5f9] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[registros]    Script Date: 3/9/2020 9:06:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[registros](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[usuario] [int] NOT NULL,
	[herramienta] [int] NOT NULL,
	[fecha] [datetime] NOT NULL,
	[type_event] [nvarchar](255) NOT NULL,
	[estado] [int] NOT NULL,
	[sincronizado] [int] NULL,
	[epc] [nvarchar](50) NULL,
 CONSTRAINT [PK_34c305689a504166a73ccaec0b0] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tools]    Script Date: 3/9/2020 9:06:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tools](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](255) NOT NULL,
	[epc] [nvarchar](255) NOT NULL,
	[toolstateId] [int] NULL,
	[image] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_e23d56734caad471277bad8bf85] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[toolstates]    Script Date: 3/9/2020 9:06:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[toolstates](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_5c0b11e8a0afd6987edb32ffd6b] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[usuarios]    Script Date: 3/9/2020 9:06:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[usuarios](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](255) NOT NULL,
	[img] [nvarchar](max) NOT NULL,
	[tipo] [int] NOT NULL,
	[lector_identificador] [int] NOT NULL,
	[estado] [int] NULL,
 CONSTRAINT [PK_d7281c63c176e152e4c531594a8] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[usuarios] ADD  CONSTRAINT [DF_a9755898fa42699a000d162010f]  DEFAULT ('0') FOR [lector_identificador]
GO
ALTER TABLE [dbo].[usuarios] ADD  DEFAULT ((1)) FOR [estado]
GO
ALTER TABLE [dbo].[tools]  WITH CHECK ADD  CONSTRAINT [FK_a6af716b526eeb6e2d27b383ed1] FOREIGN KEY([toolstateId])
REFERENCES [dbo].[toolstates] ([id])
GO
ALTER TABLE [dbo].[tools] CHECK CONSTRAINT [FK_a6af716b526eeb6e2d27b383ed1]
GO